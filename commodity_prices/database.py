import csv
import re
import sys
import sqlite3
import inspect
import logging
from datetime import datetime, timedelta, timezone
import requests
#from dateutil.parser import parse
from . import config
from . import utils

#print(parse(input_time))
#print ((config.database_filename()))


DB_SCHEMA = (
    # Version 1
    (
        # Attempt to switch to write-ahead logging for better concurrency
        "PRAGMA journal_mode=WAL",

        # Create database tables
        inspect.cleandoc("""
            CREATE TABLE metadata (
                entry TEXT NOT NULL PRIMARY KEY,
                value TEXT,
                type TEXT
            )
        """),
        inspect.cleandoc("""
            CREATE TABLE star_systems (
                name text PRIMARY KEY NOT NULL,
                pos_x REAL NOT NULL,
                pos_y REAL NOT NULL,
                pos_z REAL NOT NULL
            )
        """),
        inspect.cleandoc("""
            CREATE TABLE markets (
                market_id INT PRIMARY KEY,
                star_system TEXT,
                station TEXT
            )
        """),
        inspect.cleandoc("""
            CREATE TABLE commodities (
                timestamp TEXT NOT NULL,
                market_id INT,
                fdev_id TEXT NOT NULL,
                price INTEGER NOT NULL,
                PRIMARY KEY (market_id, fdev_id)
            )
        """),
        inspect.cleandoc("""
            CREATE TABLE average_prices (
                bubble TEXT NOT NULL,
                fdev_id TEXT NOT NULL,
                inc_fc INT,
                exc_fc INT,
                last_update TEXT NOT NULL,
                PRIMARY KEY (bubble, fdev_id)
            )
        """)
    ),
)



def get_connection():
    conn = sqlite3.connect(config.database_filename())
    return conn


def get_metadata(entry: str) -> str|int|float|None:
    """
    Get a metadata entry from the database

    Args:
        entry (str): Which entry to get.

    Returns:
        str|int|float|None: The entry's value.
    """
    conn = get_connection()
    result = conn.execute("SELECT value, type FROM metadata WHERE entry = ?", (entry,)).fetchone()
    conn.close()

    if result is None:
        return None
    value, value_type = result

    if value_type == 'boolean':
        value = bool(value)
    if value_type == 'integer':
        value = int(value)
    if value_type == 'float':
        value = float(value)
    return value


def set_metadata(entry: str, value: str|int|float|bool|None, reuse_conn: sqlite3.Connection = None):
    """
    Set a metadata entry in the database

    Args:
        entry (str): Which entry to get.
        value (str|int|float|bool|None): Value to set. Specify None to remove the entry.
        conn (sqlite3.Connection): Connection object to use. Will not automatically commit changes
            if supplied.
    """
    value_type = "string"
    if isinstance(value, bool):
        value_type = "boolean"
    if isinstance(value, int):
        value_type = "integer"
    if isinstance(value, float):
        value_type = "float"

    conn = reuse_conn
    if not reuse_conn:
        conn = get_connection()

    if value is None:
        conn.execute("DELETE FROM metadata WHERE entry = ?", (entry,))
    else:
        conn.execute("REPLACE INTO metadata VALUES (?, ?, ?)", (entry, value, value_type))

    if not reuse_conn:
        conn.commit()
        conn.close()


def update_database_schema():
    """
    Update existing database tables to a newer schema
    """
    current_schema = None
    latest_schema = len(DB_SCHEMA)

    # First, check the metadata table exists (https://stackoverflow.com/a/1604121)
    conn = get_connection()
    metadata_table_exists = conn.execute(
        "SELECT * FROM sqlite_master WHERE type='table' AND name='metadata'"
    ).fetchone()
    if not metadata_table_exists:
        # Metadata table not found, assume we're starting with an empty database
        current_schema = 0
    else:
        # Get the database's current schema
        current_schema = get_metadata('db_schema')

    # Handle unexpected schema version values
    if current_schema is None:
        logging.critical("Couldn't determine database schema version")
        sys.exit(1)
    if current_schema > latest_schema:
        logging.warning(
            "Latest database schema is version %d but database claims to be using version %d",
            latest_schema,
            current_schema,
        )


    # Update the schema if applicable
    if current_schema < latest_schema:
        logging.debug("Updating database schema from %d to %d...",
            current_schema,
            latest_schema,
        )

        # Only process updates with a version after the one the database currently uses
        for version, update in enumerate(DB_SCHEMA[current_schema:]):
            version += current_schema + 1   # arrays are 0-indexed
            for statement in update:
                logging.debug("Schema %03d: %s",
                    version,
                    re.sub(r'\n\s*', ' ', statement, flags=re.MULTILINE),
                )
                conn.execute(statement)

        set_metadata('db_schema', latest_schema, reuse_conn=conn)

        conn.commit()
    conn.close()


def update_commodity(timestamp: datetime, market_id: int, fdev_id: str, price: int):
    """
    Add or update a commodity's data.

    Args:
        timestamp (datetime): Timestamp of update.
        market_id (int): FDev internal market ID.
        fdev_id (str): FDev ID for the commodity. See
            https://github.com/EDCD/FDevIDs/blob/master/commodity.csv
        price (int): Price the market pays for this commodity at this location.
    """

    # Attempt to convert timezone to UTC, if timezone information was supplied
    if timestamp.utcoffset() != 0:
        timestamp = timestamp.astimezone(timezone.utc)

    conn = get_connection()
    conn.execute("REPLACE INTO commodities VALUES (?, ?, ?, ?)", (
        timestamp,
        market_id,
        fdev_id.lower(),
        price,
    ))
    conn.commit()
    conn.close()


def update_star(name: str, pos: list[float]):
    """
    Add or update a star's data.

    Args:
        name (str): Star system name.
        pos (list[float]): Coordinates of the system as a list of [x,y,z].
    """
    conn = get_connection()
    conn.execute("REPLACE INTO star_systems VALUES (?,?,?,?)", (
        name,
        pos[0],
        pos[1],
        pos[2],
    ))
    conn.commit()
    conn.close()


def update_market(market_id: int, star_system: str, station: str):
    """
    Add or update a market's data.

    Args:
        market_id (int): FDev ID for the market.
        star_system (str): Name of the star system the market is in.
        station (str): Station/Installation the market is at.
    """

    conn = get_connection()
    conn.execute("REPLACE INTO markets VALUES (?, ?, ?)", (
        market_id,
        star_system,
        station,
    ))
    conn.commit()
    conn.close()


def get_stored_average(fdev_id: str, bubble: str | None = None) -> tuple[int, int]:
    """
    Get a stored average price from the database

    Args:
        fdev_id (str): FDev commodity ID.

    Returns:
        tuple[float, float]: Price including fleet carriers, excluding fleet carriers.
    """
    conn = get_connection()
    result = conn.execute(
        "SELECT inc_fc, exc_fc FROM average_prices WHERE bubble = ? AND fdev_id = ?",
        (bubble.lower(), fdev_id.lower())
    )
    result = result.fetchone()
    conn.close()

    if result is None:
        return None, None
    return result


def update_stored_average(
    fdev_id: str,
    inc_fc: float,
    exc_fc: float,
    bubble: str | None = None,
    conn: sqlite3.Connection = None,
):
    """
    Store an average price in the database.

    Args:
        fdev_id (str): FDev commodity ID
        inc_fc (float): Average price including fleet carrier markets.
        exc_fc (float): Average price excluding fleet carrier markets.
        bubble (str,optional): Name of the applicable bubble. Defaults to None.
    """
    commit_and_close = False
    if not conn:
        conn = get_connection()
        commit_and_close = True
    conn.execute("REPLACE INTO average_prices VALUES (?,?,?,?,?)", (
        bubble.lower(),
        fdev_id.lower(),
        inc_fc,
        exc_fc,
        datetime.now(tz=timezone.utc),
    ))
    if commit_and_close:
        conn.commit()
        conn.close()



def get_average_price(
            commodity_id: str,
            include_fleet_carriers: bool = True,
            centre_star: str = None,
            centre_pos: tuple[float,float,float] = (None, None, None),
            distance: int = None,
        ) -> int:
    """
    Get the average selling price for the specified commodity.

    Args:
        commodity_id (str): FDev ID for the commodity. See
            https://github.com/EDCD/FDevIDs/blob/master/commodity.csv
        include_fleet_carriers (bool): Whether or not to include prices from fleet carriers.
        centre_star (str, optional): Star to centre the price lookups on. Defaults to None for
            galactic average.
        centre_pos (tuple[float,float,float], optional): Coordinates [x,y,z] to centre the price
            lookups on. Defaults to None for galactic average. If both centre_star and centre_pos
            are supplied, centre_pos will be used.
        distance (int, optional): Radius around the specified starsystem to include prices from.
            Defaults to None for galactic average.

    Returns:
        int|None: Average selling price per unit within the specified bubble of stars.
    """

    counter = 0
    total = 0

    if centre_star and None in centre_pos:
        centre_pos = get_star_pos(centre_star)

    conn = get_connection()
    data = conn.execute(
        """
        SELECT station, price, pos_x, pos_y, pos_z FROM commodities
            inner join markets on commodities.market_id = markets.market_id
            inner join star_systems on markets.star_system = star_systems.name
            where fdev_id = ?
        """,
        (commodity_id,)
    )
    for station, price, x, y, z in data:
        if not include_fleet_carriers and utils.is_fleet_carrier_id(station):
            continue

        star_pos = (x,y,z)
        if None in star_pos:
            continue

        if centre_pos and distance:
            distance_from_centre = utils.distance_between_coords(centre_pos, star_pos)
            if distance_from_centre is None or distance_from_centre > distance:
                continue

        counter += 1
        total += price

    conn.close()

    if counter > 0:
        return round(total / counter)
    return None


def get_star_pos(name: str) -> tuple[float, float, float]:
    """
    Get a star system's location from the database, if known.

    Args:
        name (str): Name of the star system.

    Returns:
        tuple[float, float, float]: Coordinates of the system as (x,y,z).
    """

    conn = get_connection()
    result = conn.execute(
        "SELECT pos_x, pos_y, pos_z FROM star_systems WHERE name = ?",
        (name,)
    ).fetchone()
    conn.close()

    if result is None:
        return (None, None, None)
    return result


def expire_old_data():
    """
    Expire data older than the configured interval from the database.
    """

    timestamp_to_expire_before = datetime.now(timezone.utc)
    timestamp_to_expire_before -= timedelta(days = config.DAYS_TO_KEEP_PRICES)
    conn = get_connection()
    conn.execute("DELETE FROM commodities WHERE timestamp < ?", (timestamp_to_expire_before,))
    conn.commit()
    conn.close()


def star_system_has_markets(star_system: str) -> bool:
    """
    Check if a star system is known to have markets.

    Args:
        star_system (str): Star system name to check

    Returns:
        bool: True if markets are known in the system, False if no markets are known to be in the
            system or if the system itself is unknown.
    """
    conn = get_connection()
    results = conn.execute(
        "SELECT timestamp FROM commodities WHERE star_system = ? LIMIT 1",
        (star_system,)
    )
    result = results.fetchone()
    conn.close()
    return bool(result is not None)


def get_stars_by_distance_from_coords(
    origin: tuple[float,float,float],
    max_distance: float = None,
) -> list:
    """
    Get a list of stars sorted by distance from the origin coordinates

    Args:
        origin (tuple[float,float,float]): _description_
        max_distance (float): _description_

    Returns:
        list[]: List of star objects
    """
    conn = get_connection()
    results = conn.execute("SELECT name, pos_x, pos_y, pos_z FROM star_systems")

    star_list = []
    for name, pos_x, pos_y, pos_z in results:
        distance = utils.distance_between_coords(origin, (pos_x, pos_y, pos_z))
        if max_distance and distance > max_distance:
            continue

        star_list.append({
            'name': name,
            'distance': distance,
        })
    conn.close()
    star_list.sort(key=lambda a: a['distance'])
    return star_list


def get_stars_by_distance(origin: str, max_distance: float = None) -> list:
    """
    Get a list of stars, sorted by distance from the named origin star

    Args:
        origin (str): _description_
        max_distance (float): _description_

    Returns:
        list[]: List of star objects
    """
    pos = get_star_pos(origin)
    if None in pos:
        return None

    return get_stars_by_distance_from_coords(pos, max_distance)


def get_commodity_ids() -> list[str]:
    """
    Get a list of all known commodity IDs

    Returns:
        list[str]: List of FDev commodity IDs.
    """
    conn = get_connection()
    results = conn.execute("SELECT DISTINCT fdev_id FROM commodities ORDER BY fdev_id")
    commodity_ids = results.fetchall()
    conn.close()
    return commodity_ids


def download_fdev_id_mappings() -> dict:
    """
    Download a mapping of FDev commodity ID to name

    Returns:
        dict: FDev commodity data
    """
    fdev_mapping = {}

    # Standard commodities
    response = requests.get(
        url='https://github.com/EDCD/FDevIDs/raw/master/commodity.csv',
        timeout=10,
    )
    if response.ok:
        for line in list(csv.DictReader(response.text.splitlines())):
            # Minor adjustments
            if line['name'] == "Void Opal":
                line['name'] = "Void Opals"
            if line['symbol'] == "Drones":
                line['category'] = None

            fdev_mapping[line['symbol'].lower()] = line

    # Rare commodities
    req = requests.get(
        url='https://raw.githubusercontent.com/EDCD/FDevIDs/master/rare_commodity.csv',
        timeout=10,
    )
    if req.ok:
        for line in list(csv.DictReader(req.text.splitlines())):
            fdev_mapping[line['symbol'].lower()] = line
            fdev_mapping[line['symbol'].lower()]['is_rare'] = True

    return fdev_mapping


def dump_prices():
    data = {}
    fdev_mapping = download_fdev_id_mappings()

    for bubble in config.KNOWN_BUBBLES:
        bubble_id = bubble['name'].lower().replace(' ', '_')

        data[bubble_id] = bubble
        data[bubble_id]['commodities'] = {}

        print(f"Processing {bubble['name']}... ", file=sys.stderr)
        for commodity_id, in get_commodity_ids():
            commodity_id = commodity_id.lower()
            print(f"    {commodity_id}                                \r", end='', file=sys.stderr)
            fc_inc_price = get_average_price(
                commodity_id,
                include_fleet_carriers=True,
                centre_pos=bubble['centre'],
                distance=bubble['radius'],
            )
            fc_exc_price = get_average_price(
                commodity_id,
                include_fleet_carriers=False,
                centre_pos=bubble['centre'],
                distance=bubble['radius'],
            )
            actual_commodity_id = commodity_id
            if commodity_id.startswith("sar_"):
                actual_commodity_id = commodity_id[4:]

            data[bubble_id]['commodities'][commodity_id] = {
                'name': fdev_mapping[actual_commodity_id]['name']
                    if actual_commodity_id in fdev_mapping
                    else commodity_id,
                'category': fdev_mapping[actual_commodity_id]['category']
                    if actual_commodity_id in fdev_mapping
                    else None,
                'fleet_carriers_included': round(fc_inc_price) if fc_inc_price else None,
                'fleet_carriers_excluded': round(fc_exc_price) if fc_exc_price else None,
            }
            if (
                actual_commodity_id in fdev_mapping and
                'is_rare' in fdev_mapping[actual_commodity_id]
            ):
                is_rare = fdev_mapping[actual_commodity_id]['is_rare']
                data[bubble_id]['commodities'][commodity_id]['is_rare'] = is_rare

    return data
