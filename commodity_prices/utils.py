import re
import math
from . import database
from . import config


def distance_between_coords(pos1, pos2):
    x = pos2[0] - pos1[0]
    y = pos2[1] - pos1[1]
    z = pos2[2] - pos1[2]
    return math.sqrt(x*x + y*y + z*z)


def distance_between_stars(star1, star2):
    pos1 = database.get_star_pos(star1)
    pos2 = database.get_star_pos(star2)
    if None in pos1 or None in pos2:
        return None
    return distance_between_coords(pos1, pos2)


def is_fleet_carrier_id(name: str) -> bool:
    return bool(re.search("^[A-Z0-9]{3}-[A-Z0-9]{3}$", name))


def closest_bubble_centre_to_coords(pos: list[float]) -> str | None:
    closest_distance = None
    closest_bubble = None
    for bubble in config.KNOWN_BUBBLES:
        distance_to_centre = distance_between_coords(pos, bubble['centre'])
        if closest_distance is None or distance_to_centre < closest_distance:
            closest_distance = distance_to_centre
            closest_bubble = bubble['name']
    return closest_bubble


def closest_bubble_centre_to_star(star_system: str) -> str | None:
    pos = database.get_star_pos(star_system)
    if None in pos:
        return None
    return closest_bubble_centre_to_coords(pos)


def closest_bubble_to_coords(pos: list[float], max_distance: float = None) -> dict | None:
    closest_distance = None
    closest_bubble = None
    for bubble in config.KNOWN_BUBBLES:
        distance_to_edge = distance_between_coords(pos, bubble['centre']) - bubble['radius']
        if closest_distance is None or distance_to_edge < closest_distance:
            closest_distance = distance_to_edge
            closest_bubble = bubble
    if (
        closest_distance is not None and
        max_distance is not None and
        closest_distance > max_distance
    ):
        return None
    return closest_bubble


def closest_bubble_to_star(star_system: str, max_distance: float = None) -> dict | None:
    pos = database.get_star_pos(star_system)
    if None in pos:
        return None
    return closest_bubble_to_coords(pos, max_distance)
