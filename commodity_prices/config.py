from environs import Env
import xdg_base_dirs


APP_NAME = 'eddn_commodity_prices'

EDDN_RELAY   = 'tcp://eddn.edcd.io:9500'
EDDN_TIMEOUT = 600000
DAYS_TO_KEEP_PRICES = 30
DEFAULT_LOG_LEVEL = 'INFO'

env = Env()
env.read_env()


def database_filename():
    data_path = env.path('EDDN_DATA_DIR', default=xdg_base_dirs.xdg_data_home() / APP_NAME)

    # Make sure the data storage dir exists
    data_path.mkdir(parents=True, exist_ok=True)

    return data_path / 'commodities.db'


def dump_filename():
    data_path = env.path('EDDN_DATA_DIR', default=xdg_base_dirs.xdg_data_home() / APP_NAME)

    # Make sure the data storage dir exists
    data_path.mkdir(parents=True, exist_ok=True)

    return data_path / 'average_prices.json'


def log_level():
    return env.log_level('EDDN_LOG_LEVEL', default=DEFAULT_LOG_LEVEL)


def gitlab_token():
    return env.str('EDDN_GITLAB_TOKEN', default=None)


# Bubbles are assumed to be spherical for simplicity, and radii are manually
# added as it takes a while to calculate them.
KNOWN_BUBBLES = [
    {
        'name'  : 'the bubble',
        'centre': [ 0, 0, 0 ],
        'radius': 320,   # Manually calculated as it takes quite a while
    },
    {
        'name'  : 'Colonia region',
        'centre': [ -9530.5, -910.28125, 19808.125 ],
        'radius': 40,   # Manually calculated
    },
]
