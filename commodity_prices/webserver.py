#!/usr/bin/env python3
import json
import logging
from flask import Flask, request
from werkzeug.middleware.proxy_fix import ProxyFix
#from get_docker_secret import get_docker_secret
from dateutil.parser import parse
from . import database
from . import utils


# Init the server module
app = Flask(__name__)
# # If we're configured behind a proxy, apply proxying fixes as per
# # https://flask.palletsprojects.com/en/2.2.x/deploying/proxy_fix/
# trusted_proxies = get_docker_secret('trust_proxies', cast_to=int)
# if trusted_proxies:
#     app.wsgi_app = ProxyFix(
#         app.wsgi_app,
#         x_for = trusted_proxies,
#         x_proto = trusted_proxies,
#         x_prefix = trusted_proxies,
#    )

def check_bool(value: str) -> bool:
    return value.lower() in ['true', 1, 'yes']


# Web server routing

@app.route('/api/commodity/<fdev_id>')
def get_commodity_price(fdev_id: str):
    location = request.args.get('location')
    include_fleet_carriers = request.args.get('fleet_carriers', type=check_bool, default=True)

    bubble = None
    price = None
    if location is not None:
        bubble = utils.closest_bubble_to_star(location, max_distance=50)

    if bubble is None:
        price = database.get_average_price(fdev_id, include_fleet_carriers=include_fleet_carriers)
    else:
        price = database.get_average_price(
            commodity_id           = fdev_id,
            centre_pos             = bubble['centre'],
            distance               = bubble['radius'],
            include_fleet_carriers = include_fleet_carriers,
        )

    data = {
        'fdev_id': fdev_id,
        'price': price,
        'bubble': bubble['name'] if bubble is not None else None,
        'includes_fleet_carriers': include_fleet_carriers,
    }
    return json.dumps(data)


@app.route('/api/sar/<fdev_id>', methods=['POST'])
def receive_sar_value(fdev_id: str):
    fdev_id = f"sar_{fdev_id}"
    print(f"Timestamp : {request.json['timestamp']}")
    print(f"Market ID : {request.json['market_id']}")
    print(f"Item      : {fdev_id}")
    print(f"Value     : {request.json['value']}")

    # Note: Value needs to be adjusted to compensate for NPC crew wages

    database.update_commodity(
        parse(request.json['timestamp']),
        request.json['market_id'],
        fdev_id,
        request.json['value'],
    )

    return '', 202


def main():
    app.run(host="0.0.0.0")
