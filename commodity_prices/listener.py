#!/usr/bin/env python3
#
# EDDN listener, adapted from https://github.com/EDCD/EDDN/tree/live/examples/
#
import time
import zlib
import logging
import zmq
import simplejson
from dateutil.parser import parse

from . import config
from . import database
from . import utils


def main():
    context = zmq.Context()
    subscriber = context.socket(zmq.SUB)

    subscriber.setsockopt(zmq.SUBSCRIBE, b"")
    subscriber.setsockopt(zmq.RCVTIMEO, config.EDDN_TIMEOUT)

    while True:
        try:
            subscriber.connect(config.EDDN_RELAY)
            logging.debug('Connected to %s', config.EDDN_RELAY)

            while True:
                message = subscriber.recv()

                # Parse message and do error handling if not received correctly
                if message is False:
                    subscriber.disconnect(config.EDDN_RELAY)
                    logging.error('Disconnected from %s due to null message', config.EDDN_RELAY)
                    break

                message = zlib.decompress(message)
                if message is False:
                    logging.error('Failed to decompress message')
                    continue

                json = simplejson.loads(message)
                if json is False:
                    logging.error('Failed to parse message as JSON')
                    continue


                # Handle commodity messages
                if json['$schemaRef'] == 'https://eddn.edcd.io/schemas/commodity/3':
                    handle_commodity_message(json)

                # Handle other messages that include a star system's position
                if (
                    'StarPos' in json['message'] and
                    ('StarSystem' in json['message'] or 'SystemName' in json['message'])
                ):
                    handle_star_position_message(json)

        except zmq.ZMQError as e:
            subscriber.disconnect(config.EDDN_RELAY)
            logging.info('Disconnected from %s due to ZMQSocketException %s', config.EDDN_RELAY, e)
            time.sleep(5)


def handle_commodity_message(msg_json: str):
    is_fleet_carrier = utils.is_fleet_carrier_id(msg_json['message']['stationName'])

    logging.info("Received commodity prices from %s in %s%s",
        msg_json['message']['stationName'],
        msg_json['message']['systemName'],
        ' [fleet carrier]' if is_fleet_carrier else ''
    )

    database.update_market(
        msg_json['message']['marketId'],
        msg_json['message']['systemName'],
        msg_json['message']['stationName'],
    )

    for commodity in msg_json['message']['commodities']:
        if commodity['buyPrice'] > 0:
            database.update_commodity(
                parse(msg_json['message']['timestamp']),
                msg_json['message']['marketId'],
                commodity['name'],
                commodity['buyPrice'],
            )


def handle_star_position_message(msg_json: str):
    system_pos = msg_json['message']['StarPos']
    system_name = None
    if 'StarSystem' in msg_json['message']:
        system_name = msg_json['message']['StarSystem']
    if 'SystemName' in msg_json['message']:
        system_name = msg_json['message']['SystemName']

    if system_name is None:
        # Should only happen if the main 'if' statement was updated but the block
        # after it that pulls the system name wasn't.
        logging.warning(
            "Couldn't identify name of star system at %s",
            str(system_pos),
        )
        logging.debug(msg_json)
        return

    #print(f"    {json['$schemaRef']}")
    database_pos = database.get_star_pos(system_name)
    if (
        system_pos[0] != database_pos[0] or
        system_pos[1] != database_pos[1] or
        system_pos[2] != database_pos[2]
    ):
        closest_bubble = utils.closest_bubble_to_coords(system_pos, max_distance=50)
        logging.info(
            "Got new position for %s system%s",
            system_name,
            " in/near " + closest_bubble['name'] if closest_bubble is not None else '',
        )
    database.update_star(system_name, msg_json['message']['StarPos'])


if __name__ == '__main__':
    main()
