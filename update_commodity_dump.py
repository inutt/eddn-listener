#!/usr/bin/env python3

import os
import sys
import json
import argparse
import logging
import gitlab
from colorlog import ColoredFormatter
from commodity_prices import config
from commodity_prices import database

# Disable warnings about variables in global scope not conforming to constant naming conventions
#pylint: disable=invalid-name

# Parse command line arguments
argparser = argparse.ArgumentParser(
    description = ("Utility to convert the accumulated EDDN data into a JSON file."),
)
argparser.add_argument(
    "--log-level", "--log", "-l",
    help="Set the severity of log messages to show"
)
args = argparser.parse_args()

if args.log_level:
    os.environ['EDDN_LOG_LEVEL'] = args.log_level


# Configure logging
log = logging.getLogger()
log_handler = logging.StreamHandler(sys.stdout)
log_formatter = ColoredFormatter(
    "%(log_color)s%(asctime)s | %(module)-8s | %(levelname)-8s | %(message)s",
    datefmt = '%b %d %H:%M:%S',
    reset = True,
    log_colors = {
        'DEBUG'   : 'bold_black',
        'INFO'    : '',
        'WARNING' : 'yellow',
        'ERROR'   : 'red',
        'CRITICAL': 'black,bg_red',
    },
    style = '%',
)
log_handler.setFormatter(log_formatter)
log.addHandler(log_handler)
log.setLevel(config.log_level())


# Process all prices across all bubbles to generate average price data.
# Note: This isn't particularly efficient, so takes quite a while.
logging.info("Generating average prices for all bubbles")
new_averages = database.dump_prices()

# Note that we need to do the commit ourselves so we can update all prices atomically and not do
# the updates if the script crashes before writing the output file.
conn = database.get_connection()
json_update_needed = False

for bubble,data in new_averages.items():
    for commodity, new_price in data['commodities'].items():
        old_inc_fc, old_exc_fc = database.get_stored_average(commodity, bubble)
        new_inc_fc = new_price['fleet_carriers_included']
        new_exc_fc = new_price['fleet_carriers_excluded']

        update_price_for_this_commodity = False
        update_reason = None
        fleet_carriers_included = None

        if old_inc_fc == new_inc_fc:
            # No update required
            pass
        elif old_inc_fc is None and new_inc_fc is not None:
            update_price_for_this_commodity = True
            update_reason = f"Previously unknown price ({old_inc_fc} -> {new_inc_fc})"
        elif new_inc_fc is None and old_inc_fc is not None:
            update_price_for_this_commodity = True
            update_reason = f"New price unknown ({old_inc_fc} -> {new_inc_fc})"
        elif not (0.9 * old_inc_fc) < new_inc_fc < (1.1 * old_inc_fc):
            update_price_for_this_commodity = True
            update_reason = f"Price changed by more than 10% ({old_inc_fc} -> {new_inc_fc})"

        if update_price_for_this_commodity:
            logging.info("%16s | %32s | FC+ | %s", bubble, commodity, update_reason)

        if old_exc_fc == new_exc_fc:
            # No update required
            pass
        elif old_exc_fc is None and new_exc_fc is not None:
            update_price_for_this_commodity = True
            update_reason = f"Previously unknown price ({old_exc_fc} -> {new_exc_fc})"
        elif new_exc_fc is None and old_exc_fc is not None:
            update_price_for_this_commodity = True
            update_reason = f"New price unknown ({old_exc_fc} -> {new_exc_fc})"
        elif not (0.9 * old_exc_fc) < new_exc_fc < (1.1 * old_exc_fc):
            update_price_for_this_commodity = True
            update_reason = f"Price changed by more than 10% ({old_exc_fc} -> {new_exc_fc})"

        if update_price_for_this_commodity:
            logging.info("%16s | %32s | FC- | %s", bubble, commodity, update_reason)

        # Update the stored average price for this commodity, but only if it met the criteria for
        # needing to be updated. This means if a price increases by below the update threshold each
        # time, we will still catch the case when the cumulative change exceeds the threshold.
        if update_price_for_this_commodity:
            json_update_needed = True   # We need to update the pricing file

            database.update_stored_average(
                bubble=bubble,
                fdev_id=commodity,
                inc_fc=new_price['fleet_carriers_included'],
                exc_fc=new_price['fleet_carriers_excluded'],
                conn=conn,
            )
        else:
            # Retrieve the previous average to store in the dump file if the commodity didn't meet
            # the criteria for needing an update so the published file and database stay consistent.
            inc, exc = database.get_stored_average(commodity, bubble)
            new_price['fleet_carriers_included'] = inc
            new_price['fleet_carriers_excluded'] = exc
            data['commodities'][commodity] = new_price


print(f"Commodity JSON update needed: {json_update_needed}")

if json_update_needed:
    file_content = json.dumps(
        new_averages,
        sort_keys=True,
        indent=4,
    )

    # Write the new prices to the output file
    logging.debug("Dumping average price data to %s", config.dump_filename())
    with open(config.dump_filename(), 'w',encoding='utf-8') as file:
        file.write(file_content)

    # Upload the new value data to the gitlab repo
    if config.gitlab_token() is None:
        logging.error("No gitlab token found, cannot commit pricing data.")
    else:
        logging.debug("Committing data to gitlab")
        gl = gitlab.Gitlab(private_token=config.gitlab_token())
        project = gl.projects.get('inutt/eddn-commodity-price-data')
        commit_data = {
            'branch': 'main',
            'commit_message': 'Commodity value update',
            'actions': [
                {
                    'action': 'update',
                    'file_path': 'average_prices.json',
                    'content': file_content,
                    'encoding': 'text',
                }
            ]
        }
        try:
            project.commits.create(commit_data)

            # Commit changes to the database, but only if we fully processed the update
            conn.commit()
        except gitlab.GitlabError as e:
            logging.error("Failed to commit data to gitlab: %s", str(e))

else:
    logging.debug("No JSON update required")

conn.close()
