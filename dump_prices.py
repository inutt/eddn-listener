#!/usr/bin/env python3

import json
from commodity_prices import database

print(json.dumps(database.dump_prices()))
